define([
  'jquery', // CashJS or jQuery
  'jquery-ui-modules/widget', // jQuery UI widget factory
  'mage/translate', // Magento translation ($t)
], function ($, widget, $t) {
  'use strict';

  // Alias the legacy $.fn.mage to $.fn so that
  // $(element).mage.estimatedDelivery(options) behaves like $(element).estimatedDelivery(options)
  $.fn.mage = $.fn.mage || $.fn;

  $.widget('mage.estimatedDelivery', {
    options: {
      isProductPage: true,
      useCustomTemplate: true,
      // Template (you can adjust as needed)
      customTemplate:
        '{if estimated_shipping_date}\n FREE Delivery: {estimated_shipping_date}</span>\n{endif}\n{if estimated_delivery_date}\n Estimated Delivery Date: {estimated_delivery_date}</span>\n{endif}',
      // Relative AJAX URL (no domain)
      url: '/prestimateddelivery/ajax/time/?isAjax=true',
      // Expected date format in your data (e.g., 'MM/DD/YYYY', 'DD/MM/YYYY', 'YYYY-MM-DD')
      dateFormat: 'MM/DD/YYYY',
    },

    // Modes for date calculation.
    modes: {
      INHERITED: '0',
      DO_NOT_SHOW: '1',
      DYNAMIC_DATE: '2',
      DYNAMIC_DATE_RANGE: '3',
      STATIC_DATE: '4',
      STATIC_DATE_RANGE: '5',
      STATIC_TEXT: '6',
      EMPTY: '7',
    },

    // Internal state.
    deliveryDates: [],
    shippingDates: [],
    calcServerTime: null,
    estimatedDelivery: null,
    estimatedShipping: null,

    _create: function () {
      // No console logs for performance/cleanliness
      this._initData();
    },

    _initData: function () {
      // Fetch localStorage data once
      var differentTime = this._getFromLocalStorage('differentTime');
      var timeCheckedAt = this._getFromLocalStorage('timeCheckedAt');
      var calcServerTime = this._getFromLocalStorage('calcServerTime');
      var browserTime = Date.now();

      this.calcServerTime = calcServerTime;
      this.timeCheckedAt = timeCheckedAt || 0; // fallback
      this.browserTime = browserTime;

      // If missing or older than 1 hour, request fresh time from server
      if (
        !differentTime ||
        !calcServerTime ||
        !timeCheckedAt ||
        timeCheckedAt + 3600000 < browserTime
      ) {
        $.ajax({
          url: this.options.url,
          type: 'POST',
          dataType: 'json',
          context: this,
          success: function (response) {
            var diffTime = browserTime - new Date(response).getTime();
            this.calcServerTime = Math.abs(diffTime - browserTime);

            this._saveToLocalStorage('differentTime', diffTime);
            this._saveToLocalStorage('timeCheckedAt', browserTime);
            this._saveToLocalStorage('calcServerTime', this.calcServerTime);
            this._renderEstimatedDate();
          },
        });
      } else {
        // Use cached server time
        this._renderEstimatedDate();
      }
    },

    _renderEstimatedDate: function () {
      var $estimatedDataEl = $('#estimated_hiden_box');
      if (!$estimatedDataEl.length) {
        // If the container is missing, exit
        return;
      }

      // Grab the JSON data that contains shipping/delivery config
      var estimatedData = JSON.parse($estimatedDataEl.text());
      // Optionally override dateFormat from the JSON
      var dateFormat = (estimatedData.dateFormat || this.options.dateFormat).toUpperCase();

      // Pull product ID from form (if on product page)
      var parentProductIdEl = document.querySelector(
        '#product_addtocart_form input[name="product"]'
      );
      var parentProductId = parentProductIdEl ? parentProductIdEl.value : null;

      // Calculate shipping/delivery data
      this._calculationEstimatedData(estimatedData, 'delivery', dateFormat);
      this._calculationEstimatedData(estimatedData, 'shipping', dateFormat);

      if (parentProductId) {
        // If we have a product ID, use product-specific data
        this.estimatedDelivery =
          this.deliveryDates[parentProductId] === this.modes.INHERITED
            ? ''
            : this.deliveryDates[parentProductId];

        this.estimatedShipping =
          this.shippingDates[parentProductId] === this.modes.INHERITED
            ? ''
            : this.shippingDates[parentProductId];

        // Trigger a custom event if needed
        var event = new CustomEvent('pr_estimated_delivery_dispatch_swatch_change', {
          detail: { parentProductId: parentProductId },
        });
        document.dispatchEvent(event);
      } else {
        // Otherwise, fall back to "category" or general data
        var catValDelivery = this._getCategoryValue(this.deliveryDates);
        if (catValDelivery !== this.modes.INHERITED) {
          this.estimatedDelivery = catValDelivery;
        }
        var catValShipping = this._getCategoryValue(this.shippingDates);
        if (catValShipping !== this.modes.INHERITED) {
          this.estimatedShipping = catValShipping;
        }
      }

      // Optionally render the custom template
      this._onDatesChange(this.estimatedDelivery, this.estimatedShipping);
    },

    _onDatesChange: function (estimatedDelivery, estimatedShipping) {
      if (this.options.isProductPage && this.options.useCustomTemplate) {
        this._renderCustomTemplate(estimatedDelivery, estimatedShipping);
      }
    },

    /**
     * Simple mini-templating function that removes blocks if variables are missing,
     * and replaces placeholders with actual data.
     */
    _render: function (template, data) {
      // Remove {if estimated_shipping_date} blocks if missing shipping
      if (!data.estimated_shipping_date) {
        template = template.replace(/{if estimated_shipping_date}[\s\S]*?{endif}/, '');
      }
      // Remove {if estimated_delivery_date} blocks if missing delivery
      if (!data.estimated_delivery_date) {
        template = template.replace(/{if estimated_delivery_date}[\s\S]*?{endif}/, '');
      }
      // Replace placeholders
      template = template
        .replace('{estimated_shipping_date}', data.estimated_shipping_date || '')
        .replace('{estimated_delivery_date}', data.estimated_delivery_date || '')
        // Clean up leftover {if}{endif}
        .replace(/{if estimated_(shipping|delivery)_date}/g, '')
        .replace(/{endif}/g, '');
      return template;
    },

    _renderCustomTemplate: function (estimatedDelivery, estimatedShipping) {
      var html = this._render(this.options.customTemplate, {
        estimated_delivery_date: estimatedDelivery,
        estimated_shipping_date: estimatedShipping,
      });
      $(this.element).html(html);
    },

    /**
     * Minimal date formatter: accepts date as a Date object or timestamp,
     * then returns a string per the requested format.
     */
    _formatDate: function (date, format) {
      var d = date instanceof Date ? date : new Date(date);

      var year = d.getFullYear();
      var month = String(d.getMonth() + 1).padStart(2, '0');
      var day = String(d.getDate()).padStart(2, '0');

      switch (format) {
        case 'YYYY-MM-DD':
          return year + '-' + month + '-' + day;
        case 'DD/MM/YYYY':
          return day + '/' + month + '/' + year;
        case 'MM/DD/YYYY':
        default:
          return month + '/' + day + '/' + year;
      }
    },

    _getDays: function (value) {
      var number = parseInt(value, 10);
      return isNaN(number) ? 0 : number;
    },

    /**
     * Unified routine that calculates either "delivery" or "shipping" data,
     * storing results in this.deliveryDates or this.shippingDates.
     */
    _calculationEstimatedData: function (estimatedData, type, dateFormat) {
      // Convert the holidays array into a set for faster lookups
      var holidaysArr = estimatedData[type] && estimatedData[type].holidays;
      var holidaySet = holidaysArr ? new Set(holidaysArr) : new Set();

      // Retrieve cutoftime if present
      var cutOfTime = (estimatedData[type] && estimatedData[type].cutoftime) || '00,00';

      // Retrieve "created_at" hour/min in "HH,mm" format
      var createdAt = this._formatDate(estimatedData.created_at, 'HH,mm');
      // Above code returns e.g. "09/02/2025" if we strictly interpret it, so you may want to parse differently.
      // If your "created_at" is a HH,mm string, skip or parse differently. Example hack:
      // let createdAt = estimatedData.created_at; // if it's already "HH,mm"

      var middleEstimatedVariable = [];

      // We'll store the difference once
      var diffBrowserTime = this.browserTime - this.timeCheckedAt;

      // Loop keys in estimatedData[type], ignoring 'cutoftime' or 'holidays'
      for (var entityId in estimatedData[type]) {
        // Skip the cutoftime/holidays properties themselves
        if (entityId === 'cutoftime' || entityId === 'holidays') {
          continue;
        }

        var itemEstimateData = estimatedData[type][entityId];
        if (!itemEstimateData) {
          // If missing or false
          continue;
        }

        // Build a reference date by adding local offset to the stored server time
        var calcDate = new Date(this.calcServerTime + diffBrowserTime);
        // Extract the hour & minute for comparison
        var calcTime = this._formatDate(calcDate, 'HH,mm');

        var bank = this._getDays(estimatedData[type][entityId + '_bank']);
        var bankRange = this._getDays(estimatedData[type][entityId + '_bank_range']);

        var enable = null;
        if (itemEstimateData === false) {
          enable = this.modes.EMPTY;
        } else if (itemEstimateData.enable) {
          enable = itemEstimateData.enable.toString();
        } else if (itemEstimateData.text) {
          enable = this.modes.STATIC_TEXT;
        }

        switch (enable) {
          case this.modes.DYNAMIC_DATE: {
            // If createdAt < cutOfTime and calcTime > cutOfTime => add an extra day
            if (createdAt < cutOfTime && calcTime > cutOfTime) {
              bank++;
            }
            this._addDaysSkippingHolidays(calcDate, bank, holidaySet);
            middleEstimatedVariable[entityId] = this._formatDate(calcDate, dateFormat);
            break;
          }
          case this.modes.DYNAMIC_DATE_RANGE: {
            var firstDate = new Date(this.calcServerTime + diffBrowserTime);
            var secondDate = new Date(this.calcServerTime + diffBrowserTime);

            if (createdAt < cutOfTime && calcTime > cutOfTime) {
              bank++;
              bankRange++;
            }
            this._addDaysSkippingHolidays(firstDate, bank, holidaySet);
            this._addDaysSkippingHolidays(secondDate, bankRange, holidaySet);

            middleEstimatedVariable[entityId] =
              this._formatDate(firstDate, dateFormat) +
              ' - ' +
              this._formatDate(secondDate, dateFormat);
            break;
          }
          case this.modes.STATIC_DATE: {
            // "from" is presumably a date string
            middleEstimatedVariable[entityId] = this._formatDate(itemEstimateData.from, dateFormat);
            break;
          }
          case this.modes.STATIC_DATE_RANGE: {
            var fromStr = this._formatDate(itemEstimateData.from, dateFormat);
            var toStr = this._formatDate(itemEstimateData.to, dateFormat);
            middleEstimatedVariable[entityId] = fromStr + ' - ' + toStr;
            break;
          }
          case this.modes.STATIC_TEXT: {
            // itemEstimateData.text is base64? decode if needed
            if (typeof itemEstimateData.text !== 'undefined') {
              middleEstimatedVariable[entityId] = decodeURIComponent(
                escape(atob(itemEstimateData.text))
              );
            } else {
              middleEstimatedVariable[entityId] = this.modes.INHERITED;
            }
            break;
          }
          case this.modes.EMPTY: {
            middleEstimatedVariable[entityId] = this.modes.INHERITED;
            break;
          }
          default: {
            // If none recognized, treat as inherited or do nothing
            middleEstimatedVariable[entityId] = this.modes.INHERITED;
            break;
          }
        }
      }

      if (type === 'delivery') {
        this.deliveryDates = middleEstimatedVariable;
      } else {
        this.shippingDates = middleEstimatedVariable;
      }
    },

    /**
     * Add "days" to date, skipping any that appear in holidaySet.
     */
    _addDaysSkippingHolidays: function (dateObj, days, holidaySet) {
      // Simple approach: add days one by one, skipping holidays
      for (var i = 0; i < days; i++) {
        dateObj.setDate(dateObj.getDate() + 1);
        // If the new date is a holiday, keep moving
        while (holidaySet.has(this._formatDate(dateObj, 'YYYY-MM-DD'))) {
          dateObj.setDate(dateObj.getDate() + 1);
        }
      }
    },

    _getCategoryValue: function (datesArray) {
      // Just gets the first non-null or zero-ish entry
      var val = datesArray.filter(function (e) {
        return e === 0 || e;
      })[0];
      return val;
    },

    // Local Storage helpers
    _getFromLocalStorage: function (key) {
      var value = localStorage.getItem(key);
      return value ? JSON.parse(value) : null;
    },
    _saveToLocalStorage: function (key, data) {
      localStorage.setItem(key, JSON.stringify(data));
    },
  });

  // Attach the backward‑compatible entry point
  $.fn.mage.estimatedDelivery = function (options) {
    return this.estimatedDelivery(options);
  };

  return $.mage.estimatedDelivery;
});
