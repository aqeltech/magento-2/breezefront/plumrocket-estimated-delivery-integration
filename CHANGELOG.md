# Change Log

## [1.1.0]

### Changed

- Replaced Plumrocket JS with a unified JS that just runs per expectations

## [1.0.0]

- Initial Release
