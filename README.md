# Plumrocket Estimated Delivery Integration

## Description

This extension does its best to integrate all storefront features of Estimated Delivery extension form Plumrocket vendor.
  
## Installation

```bash
composer require aqeltech/module-breeze-plumrocket-estimated-delivery
bin/magento module:enable AQELTech_BreezePlumrocketEstimatedDelivery
```